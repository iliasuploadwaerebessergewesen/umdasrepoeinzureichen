import numpy as np
import matplotlib as mpl
mpl.use('pgf')
import matplotlib.pyplot as plt
import matplotlib.colors
import cartopy.crs
import shapely.ops
import shapely.prepared
import shapely.geometry
import tueplots.bundles


def configure_matplotlib():
    plt.rcParams.update(tueplots.bundles.neurips2021(usetex=True))
    # xelatex is unable to plot the figure it reaches its memory limit....
    plt.rcParams["pgf.texsystem"] = "lualatex"


def plt_save(script_filename):
    plt.savefig(
        script_filename
        .replace('fig_', '')
        .replace('.py', '.pdf'),
        format='pdf')
    # the tex files gets to big for xelatex
    # and overleaf seems to use xelatex
    # (I found it somewhere on their website)
    #     .replace('.py', '.tex'),
    #     format='pgf')


def add_border_geometry(ax, border_geometry):
    ax.add_geometries(
        border_geometry,
        cartopy.crs.PlateCarree(),
        edgecolor='#242424',
        linewidth=0.5,
        facecolor='None')


def geometry_contains(geometry, longitude, latitude):
    return geometry.contains(shapely.geometry.Point(longitude, latitude))


def get_geometry_filter(longitude_grid, latitude_grid, geometry):
    geometry = shapely.prepared.prep(shapely.ops.unary_union(geometry))
    return ~np.apply_along_axis(
        lambda coordinate: geometry_contains(geometry, *coordinate),
        axis=2,
        arr=np.dstack([longitude_grid, latitude_grid]))


def apply_geometry_filter(geometry_filter, values):
    values[geometry_filter] = np.nan
    return values


def create_colormap(hex_color_ranges, *, discret=False):
    rgb_color_ranges = [
        (lower_bound, matplotlib.colors.hex2color(hex_color))
        for lower_bound, hex_color in hex_color_ranges]
    lower_bounds, rgb_colors = zip(*rgb_color_ranges)
    red, green, blue = zip(*rgb_colors)
    return matplotlib.colors.LinearSegmentedColormap(
        '',
        segmentdata={
            'red': [(lower_bounds[i],
                     red[max(0, i-1)] if discret else red[i],
                     red[i])
                    for i in range(len(lower_bounds))],
            'green': [(lower_bounds[i],
                       green[max(0, i-1)] if discret else green[i],
                       green[i])
                    for i in range(len(lower_bounds))],
            'blue': [(lower_bounds[i],
                      blue[max(0, i-1)] if discret else blue[i],
                      blue[i])
                    for i in range(len(lower_bounds))]})


def create_colormap_absolute(hex_color_ranges, maximum, **kwargs):
    hex_color_ranges = [
        (lower_bound / maximum, hex_color)
        for lower_bound, hex_color in hex_color_ranges]
    return create_colormap(hex_color_ranges, **kwargs)
    

def configure_cartesian_coordinate_system(ax):
    # credits for this function:
    # https://stackoverflow.com/a/63539077
    # Set bottom and left spines as x and y axes of coordinate system
    ax.spines['bottom'].set_position('zero')
    ax.spines['left'].set_position('zero')
    
    # Remove top and right spines
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    
    # Draw arrows
    arrow_fmt = dict(markersize=4, color='black', clip_on=False)
    ax.plot((1), (0), marker='>', transform=ax.get_yaxis_transform(), **arrow_fmt)
    ax.plot((0), (1), marker='^', transform=ax.get_xaxis_transform(), **arrow_fmt)