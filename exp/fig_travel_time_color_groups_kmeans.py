import sys; sys.path.insert(0,'..')
import src.storage as storage
from src.painting import *
import matplotlib.pyplot as plt
import matplotlib.markers
import numpy as np
import scipy.stats
import scipy.cluster
import cartopy.crs
import cartopy.feature


# disable the ShapelyDeprecationWarnings
# which are produced by cartopy as I'm using an old version
# as the newest cartopy requires libgeos 3.7.2 but debian bust only has 3.7.1
import warnings
warnings.simplefilter("ignore")


def configure_colorbar(sc, bounds, max_value, label):
    colorbar = plt.colorbar(
        sc, ticks=bounds + [max_value],
        spacing='uniform')  # ,
    #    orientation='horizontal')
    colorbar.ax.set_yticklabels(
        [fr'$\leq$ {bound:.0f}' for bound in bounds] +
        [fr'$\leq$ {max_value:.0f}'])  # ,
    #     rotation=-90)
    colorbar.set_label(label=label)


def search_cluster(data, clusters):
    maximum = max(data)
    
    for i in range(1, int(maximum // clusters)):
        initial_centroids = np.arange(clusters) * i
        codebook, distortion = scipy.cluster.vq.kmeans(data, initial_centroids)
        if codebook.shape[0] == clusters:
            return codebook, distortion
    raise ValueError(f"Can't find separation into {clusters} clusters")


def plot_map(ax, color_bounds, colors, minimum_travel_time, maximum_travel_time,
             longitude_x, latitude_y, minimum_travel_time_z, germany_geometry):
    # the coordinates were taken from https://gist.github.com/graydon/11198540
    germany_rectangle = (4.98865807458, 15.4169958839, 47.1024876979, 55.183104153)

    ax.set_extent(germany_rectangle)
    # ax.add_feature(cartopy.feature.BORDERS)
    # ax.add_feature(cartopy.feature.COASTLINE)
    add_border_geometry(ax, germany_geometry)
    
    ax.set_title('Clustered stops')

    cm = create_colormap_absolute(
        [(bound, color) for bound, color in zip(color_bounds, colors)] + [(maximum_travel_time, colors[-1])],
        maximum_travel_time,
        discret=True)
    sc = plt.scatter(
        longitude_x, latitude_y, c=minimum_travel_time_z,
        s=1,
        edgecolors='none',
        marker=matplotlib.markers.MarkerStyle('o', fillstyle='full'),
        vmin=minimum_travel_time, vmax=maximum_travel_time,
        cmap=cm,
        transform=cartopy.crs.PlateCarree())
    configure_colorbar(
        sc, color_bounds, maximum_travel_time,
        'Travel time in minutes')


def main():
    stops_title, stops = storage.load_stops(storage.get_stops_path())
    stops_title = {title: i for i, title in enumerate(stops_title)}
    trips = storage.load_trips(stops_title, stops)
    longitude_x = [trip['longitude'] for trip in trips]
    latitude_y = [trip['latitude'] for trip in trips]
    minimum_travel_time_z = [
        trip['minimum_travel_time'].total_seconds() // 60
        for trip in trips]
    germany_geometry = storage.load_germany_geometry()
    
    configure_matplotlib()
    
    minimum_travel_time = min(minimum_travel_time_z)
    maximum_travel_time = max(minimum_travel_time_z)
    time_X = np.linspace(0, maximum_travel_time, 100)
    
    # fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
    # ax3 = plt.subplot(2, 2, 3, projection=cartopy.crs.Mercator())
    
    fig = plt.figure()
    gs = fig.add_gridspec(1,1)
    # ax1 = fig.add_subplot(gs[0, 0])
    # ax3 = fig.add_subplot(gs[1, :], projection=cartopy.crs.Mercator())
    ax1 = fig.add_subplot(gs[:, :], projection=cartopy.crs.Mercator())
    
    ##### fig 1
    qualitative_colors = ('#7fc97f', '#beaed4', '#fdc086', '#ffff99', '#386cb0', '#f0027f', '#bf5b17', '#666666')
    centroids, centroids_mean_distance = search_cluster(minimum_travel_time_z, 8)
    assert all(current_centroid < next_centroid
           for current_centroid, next_centroid in zip(centroids, centroids[1:]))
    cluster_upper_bounds = [
        current_centroid + (next_centroid - current_centroid) / 2
        for current_centroid, next_centroid
        in zip(centroids, centroids[1:])]
    color_bounds = [0] + cluster_upper_bounds
    plot_map(ax1, color_bounds, qualitative_colors, minimum_travel_time, maximum_travel_time,
             longitude_x, latitude_y, minimum_travel_time_z, germany_geometry)
    
    plt_save(__file__)


if __name__ == '__main__':
    main()