import sys; sys.path.insert(0,'..')
import src.storage as storage
from src.painting import *
import matplotlib.pyplot as plt
import matplotlib.markers
import numpy as np
import scipy.stats
import scipy.cluster
import cartopy.crs
import cartopy.feature
import sklearn.linear_model


# disable the ShapelyDeprecationWarnings
# which are produced by cartopy as I'm using an old version
# as the newest cartopy requires libgeos 3.7.2 but debian bust only has 3.7.1
import warnings
warnings.simplefilter("ignore")


TUEBINGEN_HBF_LATITUDE = 48.515811
TUEBINGEN_HBF_LONGITUDE = 9.055407


def get_distance(dest_longitude, dest_latitude,
                 src_longitude=TUEBINGEN_HBF_LONGITUDE,
                 src_latitude=TUEBINGEN_HBF_LATITUDE):
    # credits: https://www.movable-type.co.uk/scripts/latlong.html
    R = 6371e3  # metres
    φ1 = src_latitude * np.pi / 180  # φ, λ in radians
    φ2 = dest_latitude * np.pi / 180
    deltaφ = (dest_latitude-src_latitude) * np.pi / 180
    deltaλ = (dest_longitude-src_longitude) * np.pi / 180

    a = (np.sin(deltaφ / 2) * np.sin(deltaφ / 2) +
         np.cos(φ1) * np.cos(φ2) *
         np.sin(deltaλ/2) * np.sin(deltaλ/2))
    c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1-a))

    return R * c  # in metres


def convert_distance_to_cluster(data, color_bounds, maximum_travel_time):
    return sum(
        (i+1) * ((lower_bound < data) & (data <= upper_bound))
        for i, (lower_bound, upper_bound) in enumerate(zip(color_bounds, color_bounds[1:] + [maximum_travel_time])))


def calculate_strict_accuracy(distance_prediction, distance_ground_truth, color_bounds, maximum_travel_time):
    prediction = convert_distance_to_cluster(
        distance_prediction,
        color_bounds, maximum_travel_time)
    ground_truth = convert_distance_to_cluster(
        distance_ground_truth,
        color_bounds, maximum_travel_time)
    accuracy = np.sum((prediction == ground_truth) * 1) / ground_truth.shape[0]
    return accuracy


def calculate_accuracy(distance_prediction, distance_ground_truth, color_bounds, maximum_travel_time):
    prediction = convert_distance_to_cluster(
        distance_prediction,
        color_bounds, maximum_travel_time)
    ground_truth = convert_distance_to_cluster(
        distance_ground_truth,
        color_bounds, maximum_travel_time)
    accuracy = np.sum((prediction >= ground_truth) * 1) / ground_truth.shape[0]
    return accuracy


def configure_colorbar(fig, sc, bounds, max_value, label):
    colorbar = fig.colorbar(
        sc, ticks=bounds + [max_value],
        spacing='uniform')  # ,
    #    orientation='horizontal')
    colorbar.ax.set_yticklabels(
        [''] +
        [fr'$\leq$ {bound:.0f}' for bound in bounds[1:]] +
        [fr'$\leq$ {max_value:.0f}'])  # ,
    #     rotation=-90)
    colorbar.set_label(label=label)


def search_cluster(data, clusters):
    maximum = max(data)
    
    for i in range(1, int(maximum // clusters)):
        initial_centroids = np.arange(clusters) * i
        codebook, distortion = scipy.cluster.vq.kmeans(data, initial_centroids)
        if codebook.shape[0] == clusters:
            return codebook, distortion
    raise ValueError(f"Can't find separation into {clusters} clusters")


def plot_map(ax, color_bounds, colors, minimum_travel_time, maximum_travel_time,
             longitude_x, latitude_y, minimum_travel_time_z, germany_geometry,
             germany_rectangle, longitude_grid, latitude_grid, travel_time_prediction,
             title):
    ax.set_extent(germany_rectangle)
    # ax.add_feature(cartopy.feature.BORDERS)
    # ax.add_feature(cartopy.feature.COASTLINE)
    add_border_geometry(ax, germany_geometry)
    
    ax.set_title(title)

    cm = create_colormap_absolute(
        [(bound, color) for bound, color in zip(color_bounds, colors)] + [(maximum_travel_time, colors[-1])],
        maximum_travel_time,
        discret=True)
    sc = plt.contourf(longitude_grid, latitude_grid, travel_time_prediction,
                 levels=color_bounds + [maximum_travel_time],
                 cmap=cm,
                 alpha=0.3,
                 transform=cartopy.crs.PlateCarree())
    plt.scatter(
        longitude_x, latitude_y, c=minimum_travel_time_z,
        s=1,
        edgecolors='none',
        marker=matplotlib.markers.MarkerStyle('.', fillstyle='full'),
        vmin=minimum_travel_time, vmax=maximum_travel_time,
        cmap=cm,
        transform=cartopy.crs.PlateCarree())
    return sc
    
    
def create_linear_regression_model_1(
        longitude_x, latitude_y, minimum_travel_time_z,
        longitude_grid, latitude_grid, germany_filter):
    model = sklearn.linear_model.LinearRegression().fit(
        np.array(list(zip(longitude_x, latitude_y))),
        minimum_travel_time_z)
    latitude_longitude_grid = np.dstack([longitude_grid, latitude_grid])
    latitude_longitude_list = latitude_longitude_grid.reshape((
        latitude_longitude_grid.shape[0] * latitude_longitude_grid.shape[1],
        latitude_longitude_grid.shape[2]))
    # travel_time_prediction = apply_geometry_filter(
    return (
        apply_geometry_filter(
            germany_filter,
            model.predict(latitude_longitude_list)
            .reshape(longitude_grid.shape)),
        model.predict(
            np.array(list(zip(longitude_x, latitude_y)))))


def create_linear_regression_model_2(
        training_long, training_lat, training_distance, training_distance_long,
        training_distance_lat, training_angle, minimum_travel_time_z,
        test_long, test_lat, test_distance, test_distance_long,
        test_distance_lat, test_angle, germany_filter, longitude_grid):
    training_data = (
        np.stack([training_long,
                  training_lat,
                  training_distance,
                  training_distance_long,
                  training_distance_lat,
                  training_angle * training_distance,],
                 axis=1))
    distance_model = sklearn.linear_model.LinearRegression(normalize=True).fit(
        training_data,
        minimum_travel_time_z)
    # travel_time_prediction = apply_geometry_filter(
    return (
        apply_geometry_filter(
            germany_filter,
            distance_model.predict(
                (np.stack(
                    [test_long,
                     test_lat,
                     test_distance,
                     test_distance_long,
                     test_distance_lat,
                     test_angle * test_distance,],
                    axis=1)))
            .reshape(longitude_grid.shape)),
        distance_model.predict(training_data))


def create_linear_regression_model_3(
        test_long, np_minimum_travel_time_z, angle_ranges, training_angle, training_distance,
        test_angle, test_distance, germany_filter, longitude_grid):
    travel_time_prediction = np.zeros_like(test_long)
    score_sum = 0
    scores_total = 0
    comparison_prediction = np.zeros_like(np_minimum_travel_time_z)

    for lower_bound, upper_bound in angle_ranges:
        training_selection = (
            (lower_bound < training_angle) &
            (training_angle <= upper_bound))
        training_X = (
            np.stack([
                training_distance[training_selection],
                ], axis=1))
        training_Y = np_minimum_travel_time_z[training_selection]
        distance_model = sklearn.linear_model.LinearRegression(normalize=True).fit(
            training_X,
            training_Y)

        test_selection = (
            (lower_bound < test_angle) &
            (test_angle <= upper_bound))
        test_X = (
            np.stack([
                test_distance[test_selection],
                ], axis=1))
        travel_time_prediction[test_selection] = (
            distance_model.predict(
                test_X))
        
        comparison_prediction[training_selection] = distance_model.predict(training_X)

    # travel_time_prediction = apply_geometry_filter(
    return (
        apply_geometry_filter(
            germany_filter,
            travel_time_prediction
            .reshape(longitude_grid.shape)),
        comparison_prediction)


def main():
    stops_title, stops = storage.load_stops(storage.get_stops_path())
    stops_title = {title: i for i, title in enumerate(stops_title)}
    trips = storage.load_trips(stops_title, stops)
    longitude_x = [trip['longitude'] for trip in trips]
    latitude_y = [trip['latitude'] for trip in trips]
    minimum_travel_time_z = [
        trip['minimum_travel_time'].total_seconds() // 60
        for trip in trips]
    np_minimum_travel_time_z = np.array(minimum_travel_time_z)
    germany_geometry = storage.load_germany_geometry()
    
    configure_matplotlib()
    fig = plt.figure()
    gs = fig.add_gridspec(1, 3)
    
    minimum_travel_time = min(minimum_travel_time_z)
    maximum_travel_time = max(minimum_travel_time_z)
    time_X = np.linspace(0, maximum_travel_time, 100)
    qualitative_colors = ('#7fc97f', '#beaed4', '#fdc086', '#ffff99', '#386cb0', '#f0027f', '#bf5b17', '#666666')
    centroids, centroids_mean_distance = search_cluster(minimum_travel_time_z, 8)
    assert all(current_centroid < next_centroid
           for current_centroid, next_centroid in zip(centroids, centroids[1:]))
    cluster_upper_bounds = [
        current_centroid + (next_centroid - current_centroid) / 2
        for current_centroid, next_centroid
        in zip(centroids, centroids[1:])]
    color_bounds = [0] + cluster_upper_bounds
    # the coordinates were taken from https://gist.github.com/graydon/11198540
    germany_rectangle = (4.98865807458, 15.4169958839, 47.1024876979, 55.183104153)
    steps = 300
    longitudes = np.linspace(germany_rectangle[2], germany_rectangle[3], steps)
    latitudes = np.linspace(germany_rectangle[0], germany_rectangle[1], steps)
    latitude_grid, longitude_grid = np.meshgrid(longitudes, latitudes)
    germany_filter = get_geometry_filter(longitude_grid, latitude_grid, germany_geometry)
    ##### fig 1
    ax1 = fig.add_subplot(gs[0, 0], projection=cartopy.crs.Mercator())
    travel_time_prediction_model_1, comparison_prediction_model_1 = create_linear_regression_model_1(
        longitude_x, latitude_y, minimum_travel_time_z,
        longitude_grid, latitude_grid, germany_filter)
    strict_accuracy_model_1 = calculate_strict_accuracy(
        comparison_prediction_model_1,
        np_minimum_travel_time_z,
        color_bounds, maximum_travel_time)
    accuracy_model_1 = calculate_accuracy(
        comparison_prediction_model_1,
        np_minimum_travel_time_z,
        color_bounds, maximum_travel_time)
    plot_map(ax1, color_bounds, qualitative_colors, minimum_travel_time, maximum_travel_time,
             longitude_x, latitude_y, minimum_travel_time_z, germany_geometry,
             germany_rectangle, longitude_grid, latitude_grid,
             travel_time_prediction_model_1,
             'Model 1')
    
    
    training_long = np.array(longitude_x)
    training_lat = np.array(latitude_y)
    training_distance = get_distance(training_long, training_lat)
    training_distance_long = get_distance(training_long, np.repeat(TUEBINGEN_HBF_LATITUDE, len(latitude_y)))
    training_distance_lat = get_distance(np.repeat(TUEBINGEN_HBF_LONGITUDE, len(longitude_x)), training_lat)
    # fix the sign such that places below have the same sign and places above have the same sign
    # (latitude corresponds to the y-coordinate -> ankathete)
    training_distance_lat *= (
         1 * (training_lat >= TUEBINGEN_HBF_LATITUDE) +
        -1 * (training_lat < TUEBINGEN_HBF_LATITUDE))
    # fix the sign such that places to the left have the same sign and places to the right have the same sign
    # (longitude corresponds to the x-coordinate -> gegenkathete)
    training_distance_long *= (
         1 * (training_long >= TUEBINGEN_HBF_LONGITUDE) +
        -1 * (training_long < TUEBINGEN_HBF_LONGITUDE))
    training_angle = np.pi + np.arctan2(training_distance_long, training_distance_lat)
    
    distance_list = get_distance(longitude_grid, latitude_grid)
    test_long = longitude_grid.reshape(-1)
    test_lat = latitude_grid.reshape(-1)
    test_distance = distance_list.reshape(-1)
    test_distance_long = get_distance(longitude_grid.reshape(-1), np.repeat(TUEBINGEN_HBF_LATITUDE, len(latitude_grid.reshape(-1))))
    test_distance_lat = get_distance(np.repeat(TUEBINGEN_HBF_LONGITUDE, len(longitude_grid.reshape(-1))), latitude_grid.reshape(-1))
    test_distance_lat *= (
         1 * (test_lat >= TUEBINGEN_HBF_LATITUDE) +
        -1 * (test_lat < TUEBINGEN_HBF_LATITUDE))
    test_distance_long *= (
         1 * (test_long >= TUEBINGEN_HBF_LONGITUDE) +
        -1 * (test_long < TUEBINGEN_HBF_LONGITUDE))
    test_angle = np.pi + np.arctan2(test_distance_long, test_distance_lat)
    ##### fig2
    ax2 = fig.add_subplot(gs[0, 1], projection=cartopy.crs.Mercator())
    travel_time_prediction_model_2, comparison_prediction_model_2 = create_linear_regression_model_2(
        training_long, training_lat, training_distance, training_distance_long,
        training_distance_lat, training_angle, minimum_travel_time_z,
        test_long, test_lat, test_distance, test_distance_long,
        test_distance_lat, test_angle, germany_filter, longitude_grid)
    strict_accuracy_model_2 = calculate_strict_accuracy(
        comparison_prediction_model_2,
        np_minimum_travel_time_z,
        color_bounds, maximum_travel_time)
    accuracy_model_2 = calculate_accuracy(
        comparison_prediction_model_2,
        np_minimum_travel_time_z,
        color_bounds, maximum_travel_time)
    plot_map(ax2, color_bounds, qualitative_colors, minimum_travel_time, maximum_travel_time,
             longitude_x, latitude_y, minimum_travel_time_z, germany_geometry,
             germany_rectangle, longitude_grid, latitude_grid, travel_time_prediction_model_2,
             'Model 2')
    
    
    angle_min = min(training_angle)
    angle_max = max(training_angle)
    angle_X = np.linspace(angle_min, angle_max, 100)
    angle_hist = scipy.stats.rv_histogram(np.histogram(training_angle, bins=len(training_angle)))
    points_per_angle_range = 60
    angle_range_share = points_per_angle_range / len(training_angle)
    angle_total_bounds = int(1 / angle_range_share)
    angle_bounds = [
        angle_hist.ppf(i * angle_range_share)
        for i in range(1, angle_total_bounds)]
    angle_ranges = [
        # exclusive, inclusive
        (lower_bound, upper_bound)
        for lower_bound, upper_bound
        in zip([angle_min - 0.1] + angle_bounds, angle_bounds + [angle_max])]
    ##### fig 3
    ax3 = fig.add_subplot(gs[0, 2], projection=cartopy.crs.Mercator())
    travel_time_prediction_model_3, comparison_prediction_model_3 = create_linear_regression_model_3(
        test_long, np_minimum_travel_time_z, angle_ranges, training_angle, training_distance,
        test_angle, test_distance, germany_filter, longitude_grid)
    strict_accuracy_model_3 = calculate_strict_accuracy(
        comparison_prediction_model_3,
        np_minimum_travel_time_z,
        color_bounds, maximum_travel_time)
    accuracy_model_3 = calculate_accuracy(
        comparison_prediction_model_3,
        np_minimum_travel_time_z,
        color_bounds, maximum_travel_time)
    sc = plot_map(ax3, color_bounds, qualitative_colors, minimum_travel_time, maximum_travel_time,
             longitude_x, latitude_y, minimum_travel_time_z, germany_geometry,
             germany_rectangle, longitude_grid, latitude_grid, travel_time_prediction_model_3,
             'Model 3')
    
    
    ##### colorbar
    configure_colorbar(
        fig, sc, color_bounds, maximum_travel_time,
        'Travel time in minutes')
    
    plt_save(__file__)
    
    print('Accuracy model 1:', accuracy_model_1)
    print('Accuracy model 2:', accuracy_model_2)
    print('Accuracy model 3:', accuracy_model_3)
    
    print('Strict accuracy model 1:', strict_accuracy_model_1)
    print('Strict accuracy model 2:', strict_accuracy_model_2)
    print('Strict accuracy model 3:', strict_accuracy_model_3)


if __name__ == '__main__':
    main()