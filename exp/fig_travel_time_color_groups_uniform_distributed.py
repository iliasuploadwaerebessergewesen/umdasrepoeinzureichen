import sys; sys.path.insert(0,'..')
import src.storage as storage
from src.painting import *
import matplotlib.pyplot as plt
import matplotlib.markers
import numpy as np
import scipy.stats
import cartopy.crs
import cartopy.feature


# disable the ShapelyDeprecationWarnings
# which are produced by cartopy as I'm using an old version
# as the newest cartopy requires libgeos 3.7.2 but debian bust only has 3.7.1
import warnings
warnings.simplefilter("ignore")


def vannotate(x, y_ratio, text, align='left', margin=10, **kwargs):
    _, y_upper_bound = plt.ylim()
    x = x + margin if align == 'left' else x - margin
    plt.text(x, y_ratio * y_upper_bound, text,
             horizontalalignment=align, **kwargs)


def vline(x, y_ratio, text, color='black', text_color=None, **kwargs):
    plt.axvline(x, color=color)
    vannotate(x, y_ratio, text + f' = {x}', color=(text_color or color), **kwargs)


def draw_quantiles(minimum_travel_time_z):
    vline(np.median(minimum_travel_time_z), 0.95, 'median', color='black')
    vline(np.quantile(minimum_travel_time_z, 0.25), 0.9, '0.25-quantile',
          color='lightgreen', text_color='green', align='right')
    vline(np.quantile(minimum_travel_time_z, 0.75), 0.85, '0.75-quantile',
          color='red', align='left')
    vline(np.quantile(minimum_travel_time_z, 0.95), 0.89, '0.975-quantile',
          color='violet', align='left')
    
    
def plot_travel_time_distribution(ax, minimum_travel_time_z, time_X, time_mean, time_variance):
    ax.set_title('Travel time distribution')
    ax.hist(minimum_travel_time_z, bins=60, density='true')
    ax.plot(time_X, scipy.stats.norm.pdf(time_X, loc=time_mean, scale=time_variance),
            label='Estimated\nnormal\ndistribution')
    ax.set_xlabel('Travel time in minutes')
    ax.set_ylabel('Likelihood')
    ax.legend()
    
    
def plot_cdf_with_color_groups(ax, color_bounds, time_mean, time_variance, maximum_travel_time, colors):
    ax.set_title('Travel time estimated cdf')
    for i, (lower_bound, upper_bound) in enumerate(zip(color_bounds, color_bounds[1:] + [maximum_travel_time])):
        time_X = np.linspace(lower_bound, upper_bound, 100)
        ax.plot(
            time_X, scipy.stats.norm.cdf(time_X, loc=time_mean, scale=time_variance),
            color=colors[i])
    
    ax.set_xlabel('Travel time in minutes')
    ax.set_ylabel('Cumulative likelihood')


def configure_colorbar(ax, sc, colors, bounds, max_value, label):
    # Well matplotlib ignores spacing=uniform BoundaryNorm does somehow work
    # to force matplotlib to draw a colorbar with the color blocks of the same size,
    # but it does not show all colors..
    # So.. to fix the overlapping labels we use fake values for the ticks
    # and overwrite their labels and color them to match their groups they belong to.
    colorbar = plt.colorbar(
        sc,
        # ticks=bounds + [max_value],
        ticks=[(i + 0.5) * max_value//len(bounds) for i in range(len(bounds))],
        spacing='uniform',
        orientation='horizontal')
    colorbar.ax.set_xticklabels(
        [fr'$\leq$ {bound:.0f}' for bound in bounds[1:]] +
        [fr'$\leq$ {max_value:.0f}'],
        rotation=-90)
    for xtick, color in zip(colorbar.ax.get_xticklabels(), colors):
        xtick.set_color(color)
    colorbar.ax.tick_params(axis='both', which='both',length=0)
    colorbar.set_label(label=label)


def plot_map(ax, color_bounds, colors,
             longitude_x, latitude_y, minimum_travel_time_z,
             minimum_travel_time, maximum_travel_time,
             germany_geometry):
    # the coordinates were taken from https://gist.github.com/graydon/11198540
    germany_rectangle = (4.98865807458, 15.4169958839, 47.1024876979, 55.183104153)

    ax.set_extent(germany_rectangle)
    # ax.add_feature(cartopy.feature.BORDERS)
    # ax.add_feature(cartopy.feature.COASTLINE)
    add_border_geometry(ax, germany_geometry)
    
    ax.set_title('Clustered stops')

    cm = create_colormap_absolute(
        [(bound, color) for bound, color in zip(color_bounds, colors)] + [(maximum_travel_time, colors[-1])],
        maximum_travel_time,
        discret=True)
    sc = ax.scatter(
        longitude_x, latitude_y, c=minimum_travel_time_z,
        vmin=minimum_travel_time, vmax=maximum_travel_time,
        cmap=cm, s=1,
        edgecolors='none',
        marker=matplotlib.markers.MarkerStyle('.', fillstyle='full'),
        transform=cartopy.crs.PlateCarree())
    configure_colorbar(
        ax, sc, colors, color_bounds, maximum_travel_time,
        'Travel time in minutes')


def main():
    stops_title, stops = storage.load_stops(storage.get_stops_path())
    stops_title = {title: i for i, title in enumerate(stops_title)}
    trips = storage.load_trips(stops_title, stops)
    longitude_x = [trip['longitude'] for trip in trips]
    latitude_y = [trip['latitude'] for trip in trips]
    minimum_travel_time_z = [
        trip['minimum_travel_time'].total_seconds() // 60
        for trip in trips]
    germany_geometry = storage.load_germany_geometry()
    
    configure_matplotlib()
    
    minimum_travel_time = min(minimum_travel_time_z)
    maximum_travel_time = max(minimum_travel_time_z)
    time_X = np.linspace(0, maximum_travel_time, 100)
    time_mean, time_variance = scipy.stats.norm.fit(minimum_travel_time_z)
    
    # fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
    # ax3 = plt.subplot(2, 2, 3, projection=cartopy.crs.Mercator())
    
    fig = plt.figure()
    gs = fig.add_gridspec(1,3)
    ax1 = fig.add_subplot(gs[0, 0])
    ax2 = fig.add_subplot(gs[0, 1])
    # ax3 = fig.add_subplot(gs[1, :], projection=cartopy.crs.Mercator())
    ax3 = fig.add_subplot(gs[0, 2], projection=cartopy.crs.Mercator())
    
    ##### fig 1
    configure_cartesian_coordinate_system(ax1)
    plot_travel_time_distribution(
        ax1, minimum_travel_time_z, time_X, time_mean, time_variance)
    
    ##### fig 2
    # We use cdf(0) as offset as there are no travel times below zero,
    # but our estimator has likelihoods >= 0 for values < 0.
    time_cdf_0 = scipy.stats.norm.cdf(0, loc=time_mean, scale=time_variance)
    color_bounds = [
        max(0, scipy.stats.norm.ppf(time_cdf_0 + i * (1-time_cdf_0)/8, loc=time_mean, scale=time_variance))
        for i in range(0, 8)]
    configure_cartesian_coordinate_system(ax2)
    diverging_colors = tuple(reversed(['#d73027', '#f46d43', '#fdae61', '#fee08b', '#d9ef8b', '#a6d96a', '#66bd63', '#1a9850']))
    qualitative_colors = ('#7fc97f', '#beaed4', '#fdc086', '#ffff99', '#386cb0', '#f0027f', '#bf5b17', '#666666')
    plot_cdf_with_color_groups(
        ax2, color_bounds, time_mean, time_variance, maximum_travel_time, qualitative_colors)
    
    ##### fig 3
    plot_map(ax3, color_bounds, qualitative_colors,
             longitude_x, latitude_y, minimum_travel_time_z,
             minimum_travel_time, maximum_travel_time,
             germany_geometry)
    
    plt_save(__file__)


if __name__ == '__main__':
    main()