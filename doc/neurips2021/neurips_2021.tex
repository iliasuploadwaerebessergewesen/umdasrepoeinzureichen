\documentclass{article}

% if you need to pass options to natbib, use, e.g.:
%     \PassOptionsToPackage{numbers, compress}{natbib}
% before loading neurips_2021

% ready for submission
\usepackage[preprint]{neurips_2021}

% to compile a preprint version, e.g., for submission to arXiv, add add the
% [preprint] option:
%     \usepackage[preprint]{neurips_2021}

% to compile a camera-ready version, add the [final] option, e.g.:
%     \usepackage[final]{neurips_2021}

% to avoid loading the natbib package, add option nonatbib:
%    \usepackage[nonatbib]{neurips_2021}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
\usepackage{xcolor}         % colors
\usepackage{pgf}

\usepackage{biblatex} %Imports biblatex package
\addbibresource{sources.bib} %Import the bibliography file


\title{Tübingen travel time map}

% The \author macro works with any number of authors. There are two commands
% used to separate the names and addresses of multiple authors: \And and \AND.
%
% Using \And between authors leaves it to LaTeX to determine where to break the
% lines. Using \AND forces a line break at that point. So, if LaTeX puts 3 of 4
% authors names on the first line, and the last on the second line, try using
% \AND instead of \And before the third author name.

\author{%
  Nico Bäurer [censored placeholder matriculation number]\\
  Eberhard Karls Universität Tübingen\\
  Tübingen, 72076 \\
  \texttt{[censored placeholder mail]} \\
  \href{https://bitbucket.org/iliasuploadwaerebessergewesen/umdasrepoeinzureichen}{https://bitbucket.org/iliasuploadwaerebessergewesen/umdasrepoeinzureichen}
  % examples of more authors
  % \And
  % Coauthor \\
  % Affiliation \\
  % Address \\
  % \texttt{email} \\
  % \AND
  % Coauthor \\
  % Affiliation \\
  % Address \\
  % \texttt{email} \\
  % \And
  % Coauthor \\
  % Affiliation \\
  % Address \\
  % \texttt{email} \\
  % \And
  % Coauthor \\
  % Affiliation \\
  % Address \\
  % \texttt{email} \\
}

\begin{document}

\maketitle

\begin{abstract}
  The aim of this project is to create a map which visualizes the travel time of public transport from ``Tübingen HBF'' to every other place within germany.
\end{abstract}

\section{Data Management}

\subsection{Data collection}

\subsubsection{Stopping points}

The dataset containing the stopping points was received from the Open-Data-Portal of the Deutschen Bahn AG\cite{OpenDataDB}.
The dataset was updated in Januar 2020 and it contains the stopping points of the Deutsche Bahn.
There are in total 6519 stops in the dataset.
To be more detailed it contains for each stop a unique identifier, reference to operating point, IFOPT (uniform stop key), the name of the stop, information about how a stop is being used (e.g. long-distance traffic, regional transport), the latitude, the longitude and whether a stop is new.
The dataset is licensed under the ``Creative Commons Attribution 4.0 International (CC BY 4.0)'' license.

\subsubsection{Trips}

There is no dataset containing the trips to the stopping points.
This is why I created a web scraper for the website of the Deutschen Bahn AG\cite{DB}.
Unfortunately their website lists only a few trips per HTTP request and it also has an aggressive rate limit.
This caused the dataset to be created rather slowly which forced me to make some trade offs in form of introducing a bias.
The dataset contains only the trips of a working day, May 2, 2022, or the trips of another working day, January 24, 2022, if there are no trips on the May 2, 2022.
So the dataset has obviously a bias as for example during school holidays the planned trips differ from the regular trips.
The extraction of all trips needed about three days while using three different internet connections to reduce the impact of the rate limit.
Their website only returned trips for 6465 different stops.
For each day all trips were extracted which had a depature time on the same day.
A trip may contain multiple vehicle changes.
A trip entry contains information about the names of the stops, prices for taking the trip, the used vehicles, the amount of changes, the departure time, the arrival time and of course the travel time.
As I created the dataset myself it is not licensed under any license.

\subsubsection{Country shapes}

The dataset containing the shape of germany and the shapes of the german states was received from gadm.org\cite{gadmorg}.
The dataset is licensed under the ``GADM license'' license.
The license forbids redistribution therefore you have to download the dataset from \href{https://gadm.org/download_country.html}{gadm.org} yourself.



\subsection{Data analysis}
\label{sec:DataAnalysis}

First of all if we want to visualize our data we have to think about how we should do that.  
In the lecture ''Scientific Visualization`` of the university Tübingen it was teached that a human can distinguish up to 8 colors well for nominal data.  
Obviously we do not work with nominal data, but we can use this as a guideline.  
Further I decided to use a qualitative color range as in this case in my opinion it was to difficult to differ and therefore to read the values of the colors of sequential and diverging color ranges.  
For the same reason I decided to use a discrete color range instead of a continuous one. \ \ Now if we look at the distribution of the travel time data we see that it seems to be normal distributed. So we can calculate an estimator for it.
One way to assign colors to times is to find disjoint subsets of the same size.
As we calculated an estimator we can simply use the quantile function (ppf) and the cumulative distribution function (cdf) to calculate the bounds of these disjoint subsets. We just have to solve $ppf(cdf(0) + i * \frac{1 - cdf(0)}{8})$ for $i\in\{1, ..., 7\}$ to get the travel time values which separate our subsets.

\begin{figure}[ht]
    \includegraphics{NeurIPS_style/fig/travel_time_color_groups_uniform_distributed.pdf}
    \caption{uniform distributed clusters}
\end{figure}

Now does such a color assignment make sense? An element of our stop set has the same likelihood to belong to one of those colors/clusters. So by looking at the colorbar we get a clue about the distribution of the travel times. E.g. if the connections were better then the first time interval (the green cluster) would be smaller. That's an interesting information, but I think for such maps you are likely more interested in knowing the places you can visit quickly. So we should try to find smaller clusters for the first few time ranges. We can try to do that with kmeans. kmeans\cite{enwiki:1067009361} roughtly works like this:

1. Define the initial centroids (points which could be in our data). E.g. random chosen values.\\
2. Assign each data point to the nearest centroid.\\
3. Assign each centroid the mean of its assigned data points.\\
4. Repeat step two and three until the centroids do not change anymore.  

A disadvantages of kmean is that it depends on the initial value of the centroids.  
In our case we can use this dependency to our advantage.  
We are interested in the places we can visit quickly and we search 8 cluster.  
So we try to choose the initial centroids such that they are as small as possible with a distance to each other such that kmeans finds 8 different clusters. One approach to do that is to set the initial centroids to $\{i * n : i \in \{0, ..., 7\}\}$ with the smallest $n \in \{1, ..., \lfloor max(dataset) / 8\rfloor \}$ for which kmeans finds 8 cluster. To get the upper limit of a time range you now simply have to calculate the mean of 2 consecutive centroids. We will use these kmeans clusters in the final result. 

\begin{figure}[ht]
    \includegraphics{NeurIPS_style/fig/travel_time_color_groups_kmeans.pdf}
    \caption{kmeans clusters}
\end{figure}



\section{Model Learning}
\subsection{Model selection}

\begin{figure}[ht]
    \includegraphics{NeurIPS_style/fig/travel_time_models.pdf}
    {\small The stops are shown as points with their true kmeans cluster.
     %The background shows the kmeans cluster predicted by the model.}
     The background shows the prediction of the model.}
    %\medskip
    \caption{prediction models}
\end{figure}

Now as we determined how we want to visualize different time ranges we need to create a model for assigning coordinates to our time range clusters. For this we will create 3 different linear models by using linear regression. The first model uses the parameters latitude and longitude. The second model uses in addition the distance, distance by longitude, distance by latitude and angle to Tübingen HBF * distance. The third model uses the quantile function (ppf) of the empirical distribution of the angles between the stops and Tübingen HBF to find angle ranges which contain 60 stops. To do this you just need to calculate the share of 60 stops and use it with the ppf to calculate the bounds of the angle ranges just like we did for travel time distribution in \nameref{sec:DataAnalysis}. Then for each of these angle ranges we use linear regression with the distance as parameter to predict the travel time.

\subsection{Model verification}

Model 1 is clearly too simple. Just by looking at it you see that all green points are classified wrongly. The model just divides germany into 7 line planes. These planes have a large variance in distances, so a lot of stops are wrongly classified.  
Model 2 isn't satisfying either. It also classifies the green points wrongly. It divides germany with ovals centered in Tübingen HBF. So the classification actually depends on the distance to Tübingen HBF which leads to better results compared to model 1.
Model 3 finally reaches good results for classifing the green points. It classifies more points correctly than model 1 and model 2, but it still classifies some points incorrectly. Some cities like Berlin seems to have a better connections than the surrounding cities. As the result only depends on the distance and the angel to Tübingen HBF the model can't predict a correct color for the surrounding cities and Berlin at the same time. This is an obvious limitation of this model.

There are two ways how you could interpret these visualization.  
The first way would be that a color only represents an upper bound for the travel time.  
For this let us define accuracy as the share of correct classifications out of the total stops.  
In this case we consider a classification as correct if for a stop a cluster was predicted with an upper bound higher or equal to the actual upper bound of the assigned cluster of a stop. In simpler words a classification is correct if predicted upper bound $\geq$ actual upper bound.
The second way to interpret these visualization would be that a color represents an upper bound and a lower bound.
Let us define strict accuracy analogous to accuracy but in this case a prediction is only classified as correct if the predicted cluster is the assigned cluster.

\begin{table}[ht]
  \caption{Accuracy comparison}
  \label{accuracy-table}
  \centering
  \begin{tabular}{lll}
    \toprule
    % \multicolumn{2}{c}{Part}                   \\
    % \cmidrule(r){1-2}
    Name     & Accuracy        & Strict accuracy \\
    \midrule
    Model 1  & 75.36\%         & 48.04\%         \\
    Model 2  & 79.63\%         & 60.09\%         \\
    Model 3  & 86.17\%         & 72.48\%         \\
    \bottomrule
  \end{tabular}
\end{table}

By looking at the table \nameref{accuracy-table} we see that the accuracies and strict accuracies support my perception of model 3 beeing the model performing the best out of the three.


%\section*{References}
\printbibliography

\end{document}
