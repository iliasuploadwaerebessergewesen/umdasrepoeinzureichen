#!/usr/bin/env python3
import sys
import re
import pathlib
import json
import time
import urllib
import unicodedata
import collections
import datetime
import csv
import traceback
import itertools
import socket

import requests
import requests_toolbelt.adapters.socket_options
import bs4


def create_soup(data):
    return bs4.BeautifulSoup(data, features="html.parser")


class ResponseError(Exception):
    pass


class InvalidLocationError(Exception):
    pass


class NoTripsOnDateError(Exception):
    pass


class SlowDown(Exception):
    def __init__(self, status_code, reason, wait_time):
        self.status_code = status_code
        self.reason = reason
        self.wait_time = wait_time


def check_response(response):
    if (response.status_code in [429, 500] or
            # they seem to serve 500 instead of 429
            str(response.status_code).startswith('5')):
        raise SlowDown(
            response.status_code,
            response.reason,
            response.headers.get(
                'Retry-After',
                response.headers.get('retry-after', 60)))
    assert response.status_code == 200
    if ('digitalData.page.error.description = "im Moment '
            'greifen zu viele Nutzer gleichzeitig auf unser Buchungssystem zu.'
            in response.content.decode(encoding=response.encoding)):
        raise SlowDown(
            response.status_code,
            response.reason,
            15)


def get(session, *args, **kwargs):
    response = session.get(*args, timeout=6, **kwargs)
    check_response(response)
    return response


def head(session, *args, **kwargs):
    response = session.head(*args, timeout=6, **kwargs)
    check_response(response)
    return response


def fetch_trips(session, date, time, source, destination):
    print('Fetching trips at', date, time)
    base_url = 'https://reiseauskunft.bahn.de/bin/query.exe/dn'
    get_params = {
        'revia': ['yes'],
        'existOptimizePrice-deactivated': ['1'],
        'country': ['DEU'],
        'dbkanal_007': ['L01_S01_D001_qf-bahn-svb-kl2_lz03'],
        'start': ['1'],
        'protocol': ['https:'],
        'S': [source],
        'Z': [destination],  # destination
        'date': [date],
        'time': [time],
        'timesel': ['depart'],
        'returnTimesel': ['depart'],
        'optimize': ['0'],
        'auskunft_travelers_number': ['1'],
        'tariffTravellerType.1': ['E'],
        'tariffTravellerReductionClass.1': ['0'],
        'tariffClass': ['2'],
        'rtMode': ['DB-HYBRID'],
        'externRequest': ['yes', 'yes'],
        'HWAI': ['JS!js=yes!ajax=yes!', 'JS!js=yes!ajax=yes!']}
    url = base_url + '?' + urllib.parse.urlencode(get_params, doseq=True)
    print('Request url:', url)
    response = get(session, url)
    print('response', response.status_code, response.reason)

    html = response.content.decode(encoding=response.encoding)
    # well.. who knows why.. but normalize does not work
    # if applied to the full html code
    # the price will still show the latin1 space \xa0
    # so apply it on each extracted information...
    normalize = lambda text: unicodedata.normalize('NFKD', text)  # noqa: E731
    soup = create_soup(html)

    # remove trips with a departure on a future day
    contained_next_day_trips = False
    removed_trips = 0
    for next_day_trip_container in soup.select(
            '#resultsOverview '
            'tr.dateDivider ~ .boxShadow.scheduledCon'):
        next_day_trip_container.decompose()
        contained_next_day_trips = True
        removed_trips += 1

    trip_containers = soup.select('.boxShadow.scheduledCon')
    trips = [
        {'source_name': normalize(
            container.select_one('.station.first').text.strip()),
         'destination_name': normalize(
             container.select_one('.station.stationDest').text).strip(),
         # Abfahrtszeit
         'departure time': normalize(
             container.select_one('.firstrow .time').text).strip(),
         # Ankunftszeit
         'arrival new days':
             (normalize((container.select_one('.time.dateChange') or
                         collections.namedtuple('placeholder', 'text')(''))
                        .text).strip()
              or '+ 0\nTag'),
         'arrival time': normalize(
             container.select_one('.last .time').text).strip(),
         'travel time': normalize(
             container.select_one('.duration.lastrow').text).strip(),
         # Umstiege
         'changes': normalize(
             container.select_one('.changes.lastrow').text).strip(),
         'vehicles': tuple(
             it.strip()
             for it in
             normalize(container.select_one('.products.lastrow').text)
             .strip().split(',')),
         # This information is only available for trips in the near future,
         # so let's remove it from our data collection.
         # 'expected utilization':
         #     normalize(container.select_one('.center.lastrow img')['src']),
         # Sparangebot (mit Zugbindung)
         'discount price': (
             normalize((container.select_one('.farePep .fareOutput') or
                        collections.namedtuple('placeholder', 'text')('N/A'))
                       .text)
             .strip()),
         # Flexpreis (keine Zugbindung)
         'price': (
             normalize((container.select_one('.fareStd .fareOutput') or
                        collections.namedtuple('placeholder', 'text')('N/A'))
                       .text)
             .strip())}
        for container in trip_containers]

    if not trips and removed_trips == 0:
        if (('Zu Ihrer Eingabe wurden mehrere '
                'm&#246;gliche Haltestellen gefunden. '
                'Bitte w&#228;hlen Sie die '
                'gew&#252;nschte Haltestelle aus.') in html or
                (f'Ihre Eingabe "{destination}" kann '
                 'nicht interpretiert werden.') in html):
            raise InvalidLocationError()
        if (('M&#246;glicherweise ist Ihr Reisewunsch '
             'an einem Datum, an dem Start oder Ziel gar '
             'nicht oder nicht mit dem gew&#228;hlten '
             'Verkehrsmittel angefahren werden') in html or
                ('Ihre Verbindungssuche verlief aufgrund '
                 'fehlender Fahrplandaten leider ergebnislos oder '
                 'unvollst&#228;ndig (Fahrplanwechsel).') in html):
            raise NoTripsOnDateError()
        raise ResponseError(
            f'no trips in the response of '
            f'date={date}, time={time}, source={source}, '
            f'destination={destination}):')

    return trips, contained_next_day_trips


TIME_FORMAT = '%H:%M'


def parse_time(time_text):
    return datetime.datetime.strptime(
        time_text, TIME_FORMAT)


def format_time(time_object):
    return time_object.strftime(TIME_FORMAT)


def fetch_day_trips_step(session, date, source, destination,
                         last_continuation, all_trips, depature_time_shift):
    last_trip = all_trips[-1]
    departure_time_text = last_trip['departure time']
    arrival_time_text = last_trip['arrival time']
    arrival_new_days_text = last_trip['arrival new days']

    query_departure_time = parse_time(departure_time_text)
    # We won't get the new trips if for example all trips have the same
    # departure time as the request will always return the same trips
    # for this departure time.
    # So we need to increase the departure time by n minutes.
    query_departure_time += datetime.timedelta(
        minutes=depature_time_shift)

    if query_departure_time.day >= 2:
        return lambda session: last_continuation(all_trips)

    last_trips, contained_next_day_trips = fetch_trips(
        session,
        date, format_time(query_departure_time),
        source, destination)

    # remove the intersection of the last request and the new request
    print('**REMOVING INTERSECTION**')
    arrival_new_days_match = re.match(
        r'\+ (?P<days>[0-9]+?)[\s\n]Tage?',
        arrival_new_days_text)
    assert arrival_new_days_match
    arrival_time = (
        parse_time(arrival_time_text) +
        datetime.timedelta(days=int(arrival_new_days_match.group('days'))))
    departure_time = parse_time(departure_time_text)
    while (last_trips and
           # the results are sorted by
           # 1. priority departure time asc,
           # 2. priority arrival time asc
           ((parse_time(last_trips[0]['departure time']) <= departure_time and
             parse_time(last_trips[0]['arrival time']) <= arrival_time) or
            parse_time(last_trips[0]['departure time']) <= departure_time)):
        trip = last_trips.pop(0)
        print(
            'removing intersection',
            f"{trip['departure time']}->{trip['arrival time']}",
            repr(trip['arrival new days']))
        print('(reason (departure time <=', departure_time_text,
              'and arrival_time <=', arrival_time, ') or',
              'departure time <=', departure_time_text, ')')
    print('new trips:',
          [(trip['departure time'],
            trip['arrival new days'],
            trip['arrival time'],
            trip['vehicles'])
           for trip in last_trips])

    if not last_trips and not contained_next_day_trips:
        # query for the current depature time only returns
        # results we already know.. so increase the depature time
        return lambda session: fetch_day_trips_step(
            session, date, source, destination,
            last_continuation, all_trips, 5 + depature_time_shift)

    all_trips += last_trips

    # implicit termination condition:
    # departure time > 23:59 (only next day trips)
    # or no more trips for today
    if last_trips:
        return lambda session: fetch_day_trips_step(
            session, date, source, destination,
            last_continuation, all_trips, 0)
    return lambda session: last_continuation(all_trips)


def fetch_day_trips(session, date, source, destination, last_continuation):
    # To have an easier control about rate limit errors
    # we use some sort of variation of the continuation software pattern.
    # We're return the continuations instead of calling them directly
    # as this allows us to execute certain parts of our code
    # multiple times as soon as an rate limit error occurs.
    # (This also has the advantage that a stack overflow can't occur)
    # Also we use the parameters of the continuations to allow to change
    # variables in this example it's the session.
    # Data is simply passed by binding it on the interpretation of a lambda
    # (interpreting a lambda into a closure).
    all_trips = []
    first_trips, _ = fetch_trips(
        session,
        date, '00:00',
        source, destination)
    last_trips = first_trips
    all_trips += last_trips

    return lambda session: fetch_day_trips_step(
        session, date, source, destination,
        last_continuation, all_trips, 0)


class DataStore(object):
    def __init__(self, path: str):
        self.path = pathlib.Path(path)
        self.tmp_path = self.path.with_suffix(
            '.tmp' + self.path.suffix)

    def load(self, default):
        data = default

        if self.tmp_path.exists():
            raise RuntimeError(
                f'temporary data file "{self.tmp_path.as_posix()}" exists. '
                'please check if data was damaged')

        try:
            with self.path.open() as f:
                data = json.load(f)
        except FileNotFoundError:
            pass

        return data

    def delete(self):
        try:
            self.path.unlink()
        except FileNotFoundError:
            pass
        try:
            self.tmp_path.unlink()
        except FileNotFoundError:
            pass

    def save(self, data):
        print('Creating temporary data file', self.tmp_path.as_posix())
        with self.tmp_path.open('w') as f:
            print('Creating/writing json array to', self.tmp_path.as_posix())
            json.dump(data, f)

        print('Unlinking original data file', self.path.as_posix())
        try:
            self.path.unlink()
        except FileNotFoundError:
            pass
        print('Renaming temporary data file to original data file')
        print(self.tmp_path.as_posix(), '->', self.path.as_posix())
        self.tmp_path.rename(self.path)
        print('Data saving completed')


def load_stops(path: str):
    with open(path) as f:
        reader = csv.reader(f, delimiter=';')
        data = list(reader)
        return (data[0], data[1:])


def create_session(network_interfaces, user_agents, counter=itertools.count()):
    i = next(counter)
    interface = (
        network_interfaces and network_interfaces[i % len(network_interfaces)])
    agent = user_agents[i % len(user_agents)]

    print('creating new http session, using interface', interface)
    session = requests.Session()
    session.headers.update({'User-Agent': agent})

    # see: https://stackoverflow.com/a/68906789
    if interface:
        options = [(socket.SOL_SOCKET, socket.SO_BINDTODEVICE, interface)]
        for prefix in ('http://', 'https://'):
            session.mount(
                prefix,
                requests_toolbelt.adapters.socket_options.SocketOptionsAdapter(
                    socket_options=options))

    return session


def scrape_trips(network_interfaces, user_agents,
                 source, destination, trip_date):
    trip_store_folder_path = pathlib.Path(trip_date) / source
    trip_store_folder_path.mkdir(parents=True, exist_ok=True)
    trip_store_path = trip_store_folder_path / destination
    trip_store = DataStore(trip_store_path.with_suffix('.json').as_posix())
    # Pause & continuing scarping trips isn't supported -> reset the saved data
    trip_store.delete()

    day_trips = []
    last_continuation = lambda results: day_trips.append(results)
    next_continuation = lambda session: fetch_day_trips(
        session,
        trip_date,
        source, destination,
        last_continuation)

    session = create_session(network_interfaces, user_agents)

    while next_continuation:
        try:
            if not network_interfaces:
                time.sleep(0.2)
            next_continuation = next_continuation(session)
        except ResponseError as e:
            print('Mhmf unknown error there are no trips in the response')
            print(e)
            time.sleep(120)
            session = create_session(network_interfaces, user_agents)
        except SlowDown as e:
            print('Whoops rate limit erreicht')
            if not network_interfaces:
                time.sleep(e.wait_time)
            time.sleep(1)
            session = create_session(network_interfaces, user_agents)
        except (requests.exceptions.ConnectTimeout,
                requests.exceptions.ConnectionError,
                requests.exceptions.ReadTimeout) as e:
            print('Timeout occured')
            print(e)
            session = create_session(network_interfaces, user_agents)
    day_trips = day_trips[0]

    # import pprint
    # pp = pprint.PrettyPrinter(indent=4)
    # pp.pprint(day_trips)
    # import sys; sys.exit(0)

    trip_store.save(day_trips)


def main():
    # usage: python3 scraper.py [network interfaces (optional)]
    # Note:
    # - Root rights are required to use different network interfaces
    #   to send the http requests.
    # - D_Bahnhof_2020_alle.CSV needs to be in the working directory.
    stops_title, stops = load_stops('D_Bahnhof_2020_alle.CSV')
    stops_title_map = {title: i for i, title in enumerate(stops_title)}
    stops_store = DataStore('stops.json')
    stops_data = stops_store.load({'progress': 0})

    TUEBINGEN_HBF_ID = '8000141'
    TRIP_DATE = 'Mo, 02.05.2022'
    ALTERNATIVE_TRIP_DATE = 'Mo, 24.01.2022'
    USER_AGENTS = (
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:95.0) Gecko/20100101 Firefox/95.0',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36',
        'Mozilla/5.0 (X11; Linux x86_64; rv:95.0) Gecko/20100101 Firefox/95.0',
        'Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0',
        'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:95.0) Gecko/20100101 Firefox/95.0',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:95.0) Gecko/20100101 Firefox/95.0')
    NETWORK_INTERFACES = tuple(
        interface.encode() for interface in sys.argv[1:])

    try:
        for i in range(stops_data['progress'], len(stops)):
            destination_id = stops[i][stops_title_map['EVA_NR']]
            destination_name = stops[i][stops_title_map['NAME']]
            if destination_id != TUEBINGEN_HBF_ID:
                print('\n\n')
                print(i + 1, "/", len(stops),
                      'Scarping trips to',
                      destination_name,
                      f"({destination_id})")
                try:
                    scrape_trips(
                        NETWORK_INTERFACES, USER_AGENTS,
                        TUEBINGEN_HBF_ID,
                        destination_id,
                        TRIP_DATE)
                except InvalidLocationError:
                    # Skip.. invalid station id
                    print('Invalid id...')
                    pass
                except NoTripsOnDateError as e:
                    # For e.g. Altenahr (8000514)
                    # there are no trips entered for the 02.05.2022 yet.
                    # If you test other dates you notice that this is the case
                    # for dates to much in the future.
                    # So let us use a date nearer to the present.. 24.01.2022
                    try:
                        scrape_trips(
                            NETWORK_INTERFACES, USER_AGENTS,
                            TUEBINGEN_HBF_ID,
                            destination_id,
                            ALTERNATIVE_TRIP_DATE)
                    except NoTripsOnDateError as e:
                        stops_without_trips = stops_data.get('stops_without_trips', None)
                        if not stops_without_trips:
                            stops_without_trips = []
                            stops_data['stops_without_trips'] = stops_without_trips
                        stops_without_trips.append({
                            'EVA_NR': destination_id, 'index': i,
                            'NAME': destination_name})
            stops_data['progress'] += 1
    except Exception as e:
        print('Exception aborted the execution:')
        print('Name:', type(e).__name__)
        print('Message:', e)
        print('Traceback:')
        print(traceback.format_exc())
    except KeyboardInterrupt:
        pass

    stops_store.save(stops_data)


if __name__ == '__main__':
    main()
