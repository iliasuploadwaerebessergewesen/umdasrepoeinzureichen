import subprocess
import pathlib
import csv
import json
import datetime
import pickle

import cartopy.io.shapereader


TUEBINGEN_HBF_ID = '8000141'


def get_repository_root():
    return pathlib.Path(
        subprocess.check_output(['git', 'rev-parse', '--show-toplevel'])
        .rstrip()
        .decode())


def get_data_path():
    return get_repository_root() / 'dat'


def get_stops_path():
    return get_data_path() / 'D_Bahnhof_2020_alle.CSV'


def get_trips_020522_path():
    return get_data_path() / 'Mo, 02.05.2022' / TUEBINGEN_HBF_ID


def get_trips_240122_path():
    return get_data_path() / 'Mo, 24.01.2022' / TUEBINGEN_HBF_ID


def get_gadmorg_path():
    return get_data_path() / 'gadm.org'


def get_germany_shape_path():
    return get_gadmorg_path() / 'gadm36_DEU_0.shp'


def get_german_states_shape_path():
    return get_gadmorg_path() / 'gadm36_DEU_1.shp'


def load_stops(path: pathlib.Path):
    with path.open() as f:
        reader = csv.reader(f, delimiter=';')
        data = list(reader)
        return (data[0], data[1:])

def load_trip(path: pathlib.Path):
    try:
        with path.open() as f:
            return json.load(f)
    except FileNotFoundError:
        return None
    
def parse_time(time_text: str):
    hours, minutes = time_text.split(':')
    return datetime.timedelta(
        hours=int(hours), minutes=int(minutes))

def load_trips(stops_title, stops):
    trips = []
    tmp_file = pathlib.Path('/tmp/trips-data-literacy')
    if tmp_file.exists():
        with tmp_file.open('rb') as f:
            return pickle.load(f)

    for stop in stops:
        stop_id = stop[stops_title['EVA_NR']]
        stop_latitude = float(
            stop[stops_title['Breite']]
            .replace(',', '.'))
        stop_longitude = float(
            stop[stops_title['Laenge']]
            .replace(',', '.'))
        trip_filename = f"{stop_id}.json"
        stop_trips = (
            load_trip(get_trips_020522_path() / trip_filename) or
            load_trip(get_trips_240122_path() / trip_filename))

        if not stop_trips:
            # there are no trips to the stop
            continue

        stop_minimum_travel_time = min(
            parse_time(trip['travel time'])
            for trip in stop_trips)
        trips.append({
            'id': stop_id,
            'latitude': stop_latitude,
            'longitude': stop_longitude,
            'minimum_travel_time': stop_minimum_travel_time})
        
    with tmp_file.open('wb') as f:
        pickle.dump(trips, f)
        
    return trips


def load_germany_geometry():
    return list(cartopy.io.shapereader.Reader(
        get_germany_shape_path().as_posix()).geometries())


def load_german_states_geometry():
    return list(cartopy.io.shapereader.Reader(
        german_states_shape_path.as_posix()).geometries())